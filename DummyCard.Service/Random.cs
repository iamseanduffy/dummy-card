﻿using System.Linq;
using System.Text;

namespace DummyCard.Service
{
    public static class Random
    {
        public static string GetRandomEvenDigit()
        {
            var rand = new System.Random();

            var randDigit = rand.Next(0, 5);

            return (randDigit*2).ToString();
        }

        public static string GetSomeRandomDigits(int numDigits)
        {
            var sb = new StringBuilder(numDigits);

            AddSomeRandomDigits(sb, numDigits);

            return sb.ToString();
        }

        public static void AddSomeRandomDigits(StringBuilder sb, int numDigits)
        {
            var rand = new System.Random();
            foreach (var i in Enumerable.Range(0, numDigits))
            {
                var randDigit = rand.Next(0, 10);
                sb.Append(randDigit);
            }
        }
    }
}