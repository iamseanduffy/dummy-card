﻿using System;
using System.Linq;
using System.Text;
using DummyCard.Service.Entities;

namespace DummyCard.Service
{
    public static class DummyCardGenerator
    {
        public static Card Generate(Bin bin)
        {
            var cardNum = new StringBuilder(bin.PanLength);
            cardNum.Append(bin.CardPrefix);
            Random.AddSomeRandomDigits(cardNum, bin.PanLength - 2 - bin.CardPrefix.Length);
            cardNum.Append(FindFinalTwoDigitsToMakeLuhnAndEvenTestsPass(cardNum.ToString()));
            AssertPassesLuhnTest(cardNum);

            return new Card
                {
                    BankInstitutionId = bin.BankInstitutionId,
                    BankName = bin.BankInstitution,
                    CardType = bin.CardType,
                    CardTypeId = bin.CardTypeId,
                    Number = cardNum.ToString()
                };
        }

        private static string FindFinalTwoDigitsToMakeLuhnAndEvenTestsPass(string cardNumSoFar)
        {
            foreach (var possiblePenultimateDigit in Enumerable.Range(0, 10))
            {
                var finalTwo = possiblePenultimateDigit + "0";
                var cardNumber = cardNumSoFar + finalTwo;
                if (IsCardNumberValid(cardNumber))
                {
                    return finalTwo;
                }
            }

            throw new Exception("Couldn't get checksum digit that makes the Luhn test pass.");
        }

        private static void AssertPassesLuhnTest(StringBuilder sb)
        {
            var cardNum = sb.ToString();
            var isCardNumberValid = IsCardNumberValid(cardNum);

            if (isCardNumberValid == false)
            {
                throw new Exception("Generated card didn't pass test");
            }
        }


        private static bool IsCardNumberValid(string cardNumber)
        {
            var checksum = GetBookLuhnChecksum(cardNumber);
            return checksum % 10 == 0;
        }

        private static int GetBookLuhnChecksum(string cardNumber)
        {
            int indicator = 1;          // will be indicator for every other number 
            int firstNumToAdd = 0;      // will be used to store sum of first set of numbers 
            int secondNumToAdd = 0;     // will be used to store second set of numbers 
            string num1;                // will be used if every other number added is greater than 10, store the left-most integer here 
            string num2;                // will be used if every other number added is greater than 10, store the right-most integer here 

            var ccArr = cardNumber.ToCharArray();

            for (int i = ccArr.Length - 1; i >= 0; i--)
            {
                char ccNoAdd = ccArr[i];

                int ccAdd = int.Parse(ccNoAdd.ToString());


                if (indicator == 1)
                {
                    // If we are on the odd number of numbers, add that number to our total 
                    firstNumToAdd += ccAdd;
                    // set our indicator to 0 so that our code will know to skip to the next piece 
                    indicator = 0;
                }
                else
                {
                    // if the current integer doubled is greater than 10 
                    // split the sum in to two integers and add them together 
                    // we then add it to our total here 
                    if ((ccAdd + ccAdd) >= 10)
                    {
                        int temporary = (ccAdd + ccAdd);
                        num1 = temporary.ToString().Substring(0, 1);
                        num2 = temporary.ToString().Substring(1, 1);
                        secondNumToAdd += (Convert.ToInt32(num1) + Convert.ToInt32(num2));
                    }
                    else
                    {
                        // otherwise, just add them together and add them to our total 
                        secondNumToAdd += ccAdd + ccAdd;
                    }
                    // set our indicator to 1 so for the next integer we will perform a different set of code 
                    indicator = 1;
                }
            }

            var checksum = firstNumToAdd + secondNumToAdd;

            return checksum;
        }
    }
}
