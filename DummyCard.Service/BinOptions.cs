﻿using System.Collections.Generic;
using DummyCard.Service.Entities;

namespace DummyCard.Service
{
    public static class BinOptions
    {
        public static readonly IList<Bin> Bins =
            (new List<Bin>
            {
                new Bin
                {
                    BankInstitution = "LLOYDS TSB",
                    BankInstitutionId = 5000,
                    CardPrefix = "490964",
                    CardType = "Visa Debit",
                    CardTypeId = 11,
                    PanLength = 16
                },
                new Bin
                {
                    BankInstitution = "BANK OF SCOTLAND",
                    BankInstitutionId = 1600,
                    CardPrefix = "675971",
                    CardType = "Switch / Maestro",
                    CardTypeId = 3,
                    PanLength = 16
                },
                new Bin
                {
                    BankInstitution = "BARCLAYS BANK",
                    BankInstitutionId = 1800,
                    CardPrefix = "465922",
                    CardType = "Visa Debit",
                    CardTypeId = 11,
                    PanLength = 16
                },

                new Bin
                {
                    BankInstitution = "HALIFAX",
                    BankInstitutionId = 3200,
                    CardPrefix = "675910",
                    CardType = "Switch / Maestro",
                    CardTypeId = 3,
                    PanLength = 16
                },

                new Bin
                {
                    BankInstitution = "HSBC",
                    BankInstitutionId = 4000,
                    CardPrefix = "675940",
                    CardType = "Switch / Maestro",
                    CardTypeId = 3,
                    PanLength = 18
                },

                new Bin
                {
                    BankInstitution = "LLOYDS TSB",
                    BankInstitutionId = 5000,
                    CardPrefix = "492182",
                    CardType = "Visa Debit",
                    CardTypeId = 11,
                    PanLength = 16
                },

                new Bin
                {
                    BankInstitution = "NATIONWIDE",
                    BankInstitutionId = 5800,
                    CardPrefix = "454313",
                    CardType = "Visa Debit",
                    CardTypeId = 11,
                    PanLength = 16
                },

                new Bin
                {
                    BankInstitution = "SANTANDER",
                    BankInstitutionId = 8700,
                    CardPrefix = "675915",
                    CardType = "Switch / Maestro",
                    CardTypeId = 3,
                    PanLength = 16
                },

            }).AsReadOnly();
    }
}