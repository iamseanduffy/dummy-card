﻿namespace DummyCard.Service.Entities
{
    public class Card
    {
        public string Number { get; set; }
        public string BankName { get; set; }
        public int BankInstitutionId { get; set; }
        public string CardType { get; set; }
        public int CardTypeId { get; set; }
    }
}
