﻿namespace DummyCard.Service.Entities
{
    /// <summary>
    /// Represents a Bank Identification Number (now Issuer Identification Number).
    /// </summary>
    public class Bin
    {
        public string CardPrefix { get; set; }
        public string BankInstitution { get; set; }
        public int BankInstitutionId { get; set; }
        public string CardType { get; set; }
        public int CardTypeId { get; set; }
        public int PanLength { get; set; }
    }
}