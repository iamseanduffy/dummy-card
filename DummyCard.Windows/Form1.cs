﻿using System;
using System.Windows.Forms;
using DummyCard.Service;

namespace DummyCard.Windows
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            itemCardTypes.SelectedIndex = 0;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Visible = false;
        }

        private void itemExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void itemGenerate_Click(object sender, EventArgs e)
        {
            var bin = BinOptions.Bins[itemCardTypes.SelectedIndex];
            var dummyCard = DummyCardGenerator.Generate(bin);
            Clipboard.SetText(dummyCard.Number);
            niMain.ShowBalloonTip(1, "","Dummy card number generated and copied to clipboard.", ToolTipIcon.Info);

        }
    }
}
