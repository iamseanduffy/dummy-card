﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DummyCard</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
		<h1>Dummy Card</h1>
		<h4>Your friendly debit card number generator.</h4>
	
        <div>
            <b>Select a card type</b><br />
            <%= GetCardTypeLinksHtml() %>
        </div>
        <br />
        <br />

        Press <b>F5</b> to refresh and get a new successful card number to use.<br /><br />
    
        <b>Bank name:</b> <%= _dummyCard.BankName %> <br />
        <b>Card type:</b> <%= _dummyCard.CardType %> <br />
        <b>Card number:</b> <%= _dummyCard.Number %> <br />
        <b>Account number:</b> <%= AccountNumber %> <br />
        <br />
        <b>NB:</b> Force a particular account number by using a URL like <a href="<%=ExampleAccountNumberUrl %>"><%=ExampleAccountNumberUrl %></a>
		<br/>
		<b>NB:</b> The bank account number used is always even, so will pass the test mode Bank Wizard check.
    </div>
    </form>
</body>
</html>
