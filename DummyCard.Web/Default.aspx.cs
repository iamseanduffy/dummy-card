﻿using System;
using System.Text;
using System.Web;
using DummyCard.Service;
using DummyCard.Service.Entities;
using Random = DummyCard.Service.Random;

public partial class _Default : System.Web.UI.Page
{
    protected Card _dummyCard;

    #region -- Properties parsed from QueryString --
    
    private const string ParamName_CardTypeIndex = "cardTypeIndex";
    protected int CardTypeIndex
    {
        get
        {
            var ret = 0;

            var sCardType = Request[ParamName_CardTypeIndex];
            int iCardType;
            if(Int32.TryParse(sCardType, out iCardType))
            {
                if(0 <= iCardType && iCardType < BinOptions.Bins.Count)
                {
                    ret = iCardType;
                }
            }

            return ret;
        }
    }

    private const string ParamName_AccountNumber = "account_number";
    private string _AccountNumber;
    protected string AccountNumber
    {
        get
        {
            if (_AccountNumber == null)
            {
                _AccountNumber = CalculateAccountNumber();
            }

            return _AccountNumber;
        }
    }

    private string CalculateAccountNumber()
    {
        var accountNumberFromQueryString = Request.QueryString[ParamName_AccountNumber];

        if (!String.IsNullOrEmpty(accountNumberFromQueryString))
        {
            return accountNumberFromQueryString;
        }

        return CalculateRandomAccountNumber();
    }

    private const string ParamName_DoSubmit = "doSubmit";
    protected bool DoSubmit
    {
        get
        {
            var ret = true;

            var sDoSubmit = Request.QueryString[ParamName_DoSubmit];
            if(sDoSubmit == "false")
            {
                ret = false;
            }

            return ret;
        }
    }

    #endregion
    
    protected void Page_Load(object sender, EventArgs e)
    {
        var bin = BinOptions.Bins[CardTypeIndex];
        _dummyCard = DummyCardGenerator.Generate(bin);
    }
    
    protected string CalculateAutoFillJavascript()
    {
        var sb = new StringBuilder();

        // JavaScript protocol prefix: the magic that makes it all happen!
        sb.Append("javascript:");

        // Start function wrapper. Needed for IE.
        sb.Append("(function(){");

        sb.Append(GetSingleSetJavaScript("uiCardInstitutionName", _dummyCard.BankName));
        sb.Append(GetSingleSetJavaScript("uiCardType", _dummyCard.CardTypeId.ToString()));
        sb.Append(GetSingleSetJavaScript("uiCardNumber", _dummyCard.Number));

        sb.Append(GetSingleSetJavaScript("uiStartDate1", "01"));
        sb.Append(GetSingleSetJavaScript("uiStartDate2", DateTime.Now.Year.ToString()));
        sb.Append(GetSingleSetJavaScript("uiExpiryDate1", "01"));
        sb.Append(GetSingleSetJavaScript("uiExpiryDate2", (DateTime.Now.Year+3).ToString()));
        sb.Append(GetSingleSetJavaScript("uiCV2", "123"));
        
        sb.Append(GetSingleSetJavaScript("uiCardAccountName", "account name"));
        sb.Append(GetSingleSetJavaScript("uiCardAccountNumber", AccountNumber));
        sb.Append(GetSingleSetJavaScript("uiCardSortCode1", "12"));
        sb.Append(GetSingleSetJavaScript("uiCardSortCode2", "23"));
        sb.Append(GetSingleSetJavaScript("uiCardSortCode3", "34"));

        // Check "copy bank details from my card".
        sb.Append("$('#uiCopyFromAbove').click();");

        if (DoSubmit)
        {
            // Submit the form.
            sb.Append("$('#submit').click();");
            sb.Append("setTimeout(function(){__doPostBack('submit','');}, 200);");
        }

        // End function wrapper.
        sb.Append("})();");

        return sb.ToString();
    }

    /// <remarks>
    ///     This doesn't perform any escaping of the strings,
    ///     so don't pass in any control characters like ', ", \
    /// </remarks>
    private string GetSingleSetJavaScript(string controlId, string value)
    {
        return string.Format(
            "$('#{0}').val('{1}');",
            controlId,
            value);
    }

    protected string ExampleAccountNumberUrl
    {
        get
        {
            return 
                String.Format(
                    "{0}?{1}=12341234",
                    Request.Url.GetLeftPart(UriPartial.Path),
                    ParamName_AccountNumber);
        }
    }

    private string CalculateRandomAccountNumber()
    {
        var accountNum = Random.GetSomeRandomDigits(7);
        accountNum += Random.GetRandomEvenDigit();

        return accountNum;
    }

    protected string GetCardTypeLinksHtml()
    {
        var rv = new StringBuilder();

        var bins = BinOptions.Bins;
        
        for(int ix = 0; ix < bins.Count; ix++)
        {
            var bin = bins[ix];

            var message =
                String.Format("{0} - {1} ({2})",
                    bin.BankInstitution,
                    bin.CardType,
                    bin.CardPrefix);

            var linkTarget =
                String.Format(
                    "{0}?{1}={2}",
                    Request.Url.GetLeftPart(UriPartial.Path),
                    ParamName_CardTypeIndex,
                    ix
                    );

            var htmlEncMessage = HttpUtility.HtmlEncode(message);
            var htmlAttrEncLink = HttpUtility.HtmlAttributeEncode(linkTarget);

            if(ix != CardTypeIndex)
            {
                // Show link
                rv.AppendFormat(
                    "<a href='{0}'>{1}</a>",
                    htmlAttrEncLink,
                    htmlEncMessage
                    );
            }
            else
            {
                // Just show copy
                rv.AppendFormat(
                    "{0} (selected)",
                    htmlEncMessage
                    );
            }

            rv.Append("<br/>");
        }

        return rv.ToString();
    }
}
